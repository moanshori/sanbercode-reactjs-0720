// Soal 1
var i = 2;
console.log("LOOPING PERTAMA");
while(i<=20){
    console.log(i + " = I Love coding");
    i+=2;
}

i = 20;
console.log("LOOPING KEDUA");
while(i>=2){
    console.log(i + " = I will become a frontend developer");
    i-=2;
}

// Soal 2
for (var i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
        console.log(i + " - I Love Coding");
    } else if (i % 2 == 1) {
        console.log(i + " - Santai");
    } else if (i % 2 == 0) {
        console.log(i + " - Berkualitas");
    }
}

// Soal 3
for (var i = 0; i < 7; i++){
    var s = "";
    for (var j = 0; j <= i; j++){
        s += "#";
    }
    console.log(s);
}

// Soal 4
var kalimat="saya sangat senang belajar javascript"
var arrKalimat = kalimat.split(' ');
console.log(arrKalimat);

// Soal 5
// sort array
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

for (var i = 0; i < daftarBuah.length; i++){
    console.log(daftarBuah.sort()[i]);
}