// Soal 1
// membuat function Halo dengan return 

function halo(){
    return "\"Halo Sanbers!\""
}
console.log(halo()) // "Halo Sanbers!" 

// Soal 2
// membuat function kalikan dengan parameter dan return nilai

function kalikan(a, b){
    return a*b;
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

// Soal 3
// membuat function introduce

function introduce(name, age, address, hobby) {
    return "\"Nama saya " + name +
        ", umur saya " + age +
        ", alamat saya di " + address +
        ", dan saya punya hobby yaitu " + hobby +
        "!\""
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)