// Soal 1 
// menggabungkan variabel menjadi kalimat
console.log("soal 1");

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(kataPertama + " " + kataKedua.charAt(0).toUpperCase() + kataKedua.substring(1) + " " + kataKetiga + " " + kataKeempat.toUpperCase());

// Soal 2
// ubah tipe data variabel lalu jumlahkan semuanya dan tampilkan hasilnya
console.log("\nsoal 2");
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

kataPertama = Number(kataPertama);
kataKedua = Number(kataKedua);
kataKetiga = Number(kataKetiga);
kataKeempat = Number(kataKeempat);

console.log(kataPertama + kataKedua + kataKetiga + kataKeempat);

// Soal 3
// selesaikan variabel yg belum terisi sehingga output sesuai dg yg ditentukan
console.log("\nsoal 3");

var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);; // do your own! 
var kataKetiga = kalimat.substring(15, 18);; // do your own! 
var kataKeempat = kalimat.substring(19, 24);; // do your own! 
var kataKelima = kalimat.substring(25);; // do your own! 

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

// Soal 4 
// buat variabel nilai dg angka dari 0 sampai 100, misal 75
// buat lah pengkondisian dengan if-elseif dengan kondisi
console.log("\nsoal 4");

var nilai = 57;
if (nilai >= 80) {
    console.log(nilai + " Indeksnya A")
} else if (nilai >= 70 && nilai < 80) {
    console.log(nilai + " Indeksnya B")
} else if (nilai >= 60 && nilai < 70) {
    console.log(nilai + " Indeksnya C")
} else if (nilai >= 50 && nilai < 60) {
    console.log(nilai + " Indeksnya D")
} else if (nilai < 50) {
    console.log(nilai + " Indeksnya F")
}

// Soal 5
// buat variabel tanggal, bulan, tahun dan isi sesuai tanggal lahir
// buat switch case
console.log("\nsoal 5");

var tanggal = 1;
var bulan = 8;
var tahun = 1994;

switch (bulan) {
    case 1: {
        console.log(tanggal + " Januari " + tahun);
        break;
    }
    case 2: {
        console.log(tanggal + " Februari " + tahun);
        break;
    }
    case 3: {
        console.log(tanggal + " Maret " + tahun);
        break;
    }
    case 4: {
        console.log(tanggal + " April " + tahun);
        break;
    }
    case 5: {
        console.log(tanggal + " Mei " + tahun);
        break;
    }
    case 6: {
        console.log(tanggal + " Juni " + tahun);
        break;
    }
    case 7: {
        console.log(tanggal + " Juli " + tahun);
        break;
    }
    case 8: {
        console.log(tanggal + " Agustus " + tahun);
        break;
    }
    case 9: {
        console.log(tanggal + " September " + tahun);
        break;
    }
    case 10: {
        console.log(tanggal + " Oktober " + tahun);
        break;
    }
    case 11: {
        console.log(tanggal + " November " + tahun);
        break;
    }
    case 12: {
        console.log(tanggal + " Desember " + tahun);
        break;
    }

}