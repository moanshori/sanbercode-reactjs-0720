// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Tulis code untuk memanggil function readBooks di sini
var i = 0;
function read(t, b) {
    if (i < books.length) {
        readBooks(t, books[i], function (check) {
            i = i + 1;
            read(check, books[i]);
        });
    }
}

read(10000, books);