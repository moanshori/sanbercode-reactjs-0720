var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
// readBooksPromise(10000, books[0]);
var i = 0;
function read(t, b) {
    if (i < books.length) {
        readBooksPromise(t, books[i]).then(function (check) {
            i = i + 1;
            read(check, books[i]);
        }).catch(function (check) {
            console.log(check);
        })
    }
}

read(10000, books);
