//Soal 1
// ubah array dibawah menjadi object
// var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var daftarPeserta = {
    nama: "Asep",
    jenisKelamin: "laki-laki",
    hobi: "baca buku",
    tahunLahir: 1992,
}
console.log(daftarPeserta);

//Soal 2
var arrObject = [
    {
        nama: "Strawberry",
        warna: "merah",
        adaBiji: "tidak",
        harga: 9000
    },
    {
        nama: "Jeruk",
        warna: "oranye",
        adaBiji: "ada",
        harga: 8000
    },
    {
        nama: "Semangka",
        warna: "Hijau & Merah",
        adaBiji: "ada",
        harga: 10000
    },
    {
        nama: "Pisang",
        warna: "Kuning",
        adaBiji: "tidak",
        harga: 5000
    },
];
console.log(arrObject[0]);

//Soal 3
var dataFilm = [];

function tambahFilm(n, d, g, t){
    var obj = {
        nama: n,
        durasi: d,
        genre: g,
        tahun: t,
    }
    return dataFilm.push(obj);
}

tambahFilm("Pretty Boys", "1 Jam 30 menit", "Commedy, SLice of Life", 2019);
tambahFilm("Beranak Dalam Kubur", "59 menit", "Horror", 2003);
console.log(dataFilm);

//Soal 4
//Soal 4 release 0

class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }

    get gname(){
        return this.name;
    }

    get glegs(){
        return this.legs;
    }

    get gcold_blooded(){
        return this.cold_blooded;
    }
}

var sheep = new Animal("shaun");
// console.log(sheep.name); // "shaun"
// console.log(sheep.legs); // 4
// console.log(sheep.cold_blooded); // false
console.log(sheep.gname);
console.log(sheep.glegs);
console.log(sheep.gcold_blooded);

//Soal4 release 1
class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump(){
        console.log("hop hop");
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }
    yell(){
        console.log("Auooo");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

//Soal 5
class Clock {
    timer;
    constructor({template}) {
        this.template = template;
    }

    render() {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(()=> this.render(), 1000)
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();  