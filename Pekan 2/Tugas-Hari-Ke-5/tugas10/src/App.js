import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const buah = [
    "Semangka",
    "Jeruk",
    "Nanas",
    "Salak",
    "Anggur"
  ];
  return (
    <div className="App">
      <h1>Hello World</h1>
      <div class="content">
        <table>
          <tr>
            <th>Nama Pelanggan</th>
            <td><input></input></td>
          </tr>
          <tr>
            <th>
              Daftar Item
            </th>
            <td>
              <div>
                <input type="checkbox" /><label>Semangka</label>
              </div>
              <div>
                <input type="checkbox" /><label>Jeruk</label>
              </div>
              <div>
                <input type="checkbox" /><label>Nanas</label>
              </div>
              <div>
                <input type="checkbox" /><label>Salak</label>
              </div>
              <div>
                <input type="checkbox" /><label>Anggur</label>
              </div>
            </td>
          </tr>
          <tr>
            <th>
              <a href="#">
                <input class="btn" type="button" value="Kirim" />
              </a>
            </th>
          </tr>
        </table>
      </div>
    </div>
  );
}

export default App;
