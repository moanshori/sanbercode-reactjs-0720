// Soal 1
console.log("\nSoal 1 -------------------------------")
const luasLingkaran = (r) => {
    let phi = 3.14;
    return phi * r * r;
}

const kelilingLingkaran = (r) => {
    let phi = 3.14;
    return 2 * phi * r;
}

console.log(luasLingkaran(7));
console.log(kelilingLingkaran(7));

// Soal 2
console.log("\nSoal 2 -------------------------------")

let kalimat = "";

const tambahKata = (word) => {
    // console.log();
    kalimat += `${word} `;
}

tambahKata("saya");
tambahKata("adalah");
tambahKata("seorang");
tambahKata("frontend");
tambahKata("developer");
console.log(kalimat);

// Soal 3
console.log("\nSoal 3 -------------------------------")
class Book {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }

    get bname() {
        return this.name;
    }

    get btotalPage() {
        return this.totalPage;
    }

    get bprice() {
        return this.price;
    }
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this.isColorful = isColorful;
    }

    get kisColorful(){
        return this.isColorful;
    }
}

bukuku = new Book("Developing React JS", 23, 5000);
console.log(bukuku);
komikku = new Komik("Uyee Man", 100, 10000, true);
console.log(komikku);